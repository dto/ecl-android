(in-package :cl-user)

(require '#:asdf)
(require '#:sockets)
(require '#:serve-event)

(setf asdf:*user-cache* (merge-pathnames #P"./asdf-cache/" *default-pathname-defaults*))
(pushnew (namestring *default-pathname-defaults*) asdf:*central-registry*)

;; ;; paths for rest of xelf and game
(push "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/"
      asdf:*central-registry*)
(push "/data/data/org.lisp.ecl/app_resources/home/systems/3x0ng/"
      asdf:*central-registry*)
(push "/data/data/org.lisp.ecl/app_resources/home/systems/inv8r/"
      asdf:*central-registry*)

;; ;; load profiler function
;; (defun load-profiler ()
;;   (push "/data/data/org.lisp.ecl/app_resources/home/systems/metering/"
;; 	asdf:*central-registry*)
;;   (load #P"/data/data/org.lisp.ecl/app_resources/home/systems/metering/metering.fas"))

;; (handler-bind ((warning #'muffle-warning))
;;   (load-profiler))

(defvar *ecl-home* *default-pathname-defaults*)

(defun find-lisps (system)
  (remove-duplicates 
   (delete nil
	   (mapcar #'(lambda (p)
		       (when (typep (cdr p) (find-class 'asdf:cl-source-file))
			 (asdf:component-pathname (cdr p))))
		   (asdf/plan:plan-actions (asdf:make-plan 'asdf:sequential-plan 'asdf:load-op system))))))

(defun fasc-name (path)
  (let* ((s (namestring path))
	 (period (position #\. s :from-end t))
	 (base (subseq s 0 period))
	 (cache-prefix "/data/data/org.lisp.ecl/app_resources/asdf-cache"))
    (concatenate 'string cache-prefix base ".fasc")))

(defun find-fascs (system)
  (mapcar #'fasc-name (find-lisps system)))

(defun load-fascs (&optional (fascs (find-fascs)))
  (dolist (fasc fascs)
    (load fasc :verbose t)))

(defun find-base-fascs ()
  (remove-duplicates (mapcan #'find-fascs
			     '(:alexandria :babel :trivial-garbage :cffi :cl-opengl :cl-fad :lispbuilder-sdl
			       :lispbuilder-sdl-image :lispbuilder-sdl-mixer :lispbuilder-sdl-gfx :lispbuilder-sdl-ttf))))

(defparameter *fascs*
  '(
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/bin/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/bin/globals.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/library.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/util.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/cffi-util.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/cffi-translate.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/endian.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/version.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/stdinc.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/mutex.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/timer.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/error.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/rwops.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/audio.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/cdrom.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/joystick.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/active.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/keysym.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/mouse.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/events.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/syswm.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/video.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/sdl.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/glue.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/cffi/documentation.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl-image/image-library.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl-image/sdl-image.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl-gfx/gfx-library.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/base/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/base/pixel.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/base/util.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/base/rectangle.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/base/surfaces.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/base/rwops.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/base/sdl-util.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/assets/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/assets/globals.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/globals.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/generics.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/base.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/util.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/init.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/mouse.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/input-util.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/fps.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/events.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/primitives.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/color.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/point.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/rectangle.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/pixel.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/surfaces.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/video.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/rwops.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/image.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/sdl-gfx.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/drawing-primitives.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/font-definition.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-definition.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/gfx-font-definition.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/simple-font-definition.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/font.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-5x7.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-5x8.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-6x9.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-6x10.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-6x12.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-6x13.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-6x13b.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-6x13o.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-7x13.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-7x13b.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-7x13o.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-7x14.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-7x14b.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-8x8.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-8x13.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-8x13b.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-8x13o.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-9x15.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-9x15b.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-9x18.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-9x18b.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font-data-10x20.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/simple-font-data.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/bitmap-font.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/simple-font.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/gfx-font.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/string-solid.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/string-shaded.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/string-blended.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/gfx-string-solid.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/gfx-string-shaded.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/keys.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/default-colors.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/audio.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/mixer.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/active.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/version.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl/sdl/fill.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-image/bin/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-image/bin/globals.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-image/cffi/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-image/cffi/library.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-image/cffi/image.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-image/cffi/translate.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-image/sdl-image/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-image/sdl-image/generics.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-image/sdl-image/sdl-image-util.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/bin/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/bin/globals.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/cffi/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/cffi/library.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/cffi/ttf.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/cffi/translate.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/sdl-ttf/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/sdl-ttf/generics.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/sdl-ttf/globals.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/sdl-ttf/ttf-font-definition.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/sdl-ttf/ttf-font-data.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/sdl-ttf/sdl-util-ttf.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/sdl-ttf/ttf-font.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/sdl-ttf/string-solid.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/sdl-ttf/string-shaded.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-ttf/sdl-ttf/string-blended.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-mixer/bin/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-mixer/bin/globals.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-mixer/cffi/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-mixer/cffi/library.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-mixer/cffi/mixer.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-mixer/cffi/sdl-mixer.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-mixer/cffi/translate.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-mixer/sdl-mixer/package.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-mixer/sdl-mixer/globals.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/lispbuilder-20151218-git/lispbuilder-sdl-mixer/sdl-mixer/mixer.fasc"
 ;; "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/bordeaux-threads-v0.8.5/src/pkgdcl.fasc"
 ;; "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/bordeaux-threads-v0.8.5/src/bordeaux-threads.fasc"
 ;; "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/bordeaux-threads-v0.8.5/src/impl-ecl.fasc"
 ;; "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/dists/quicklisp/software/bordeaux-threads-v0.8.5/src/default-implementations.fasc"
))

(defparameter *gl-fascs*
  '("/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/bindings-package.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/constants.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/library.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/bindings.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/types.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/funcs-gl-glcore-gles1-gles2.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/funcs-gl-glcore-gles2.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/funcs-gl-glcore.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/funcs-gl-gles1-gles2.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/funcs-gl-gles1.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/funcs-gl-gles2.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/funcs-gl.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/funcs-gles1-gles2.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/funcs-gles1.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/funcs-gles2.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/package.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/util.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/opengl.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/rasterization.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/framebuffer.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/special.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/state.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/dsa.fasc"
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/cl-opengl/gl/extensions.fasc"))

(defparameter *xelf-lisps*
  '("/data/data/org.lisp.ecl/app_resources/home/systems/xelf/xelf.lisp"
    "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/rgb.lisp"
    "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/keys.lisp"
    "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/math.lisp"
    "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/logic.lisp"
    "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/prototypes.lisp"
    "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/console.lisp"
    "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/blocks.lisp"
    "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/buffers.lisp"
    "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/path.lisp"
    "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/command.lisp"
    "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/upnp.lisp"
    "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/netplay.lisp"
    "/data/data/org.lisp.ecl/app_resources/home/systems/xelf/generics.lisp"))
  
(defparameter *xelf-fascs* '(
    "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/xelf.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/rgb.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/keys.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/math.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/logic.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/prototypes.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/console.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/blocks.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/buffers.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/path.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/command.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/upnp.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/netplay.fasc"
 "/data/data/org.lisp.ecl/app_resources/asdf-cache/data/data/org.lisp.ecl/app_resources/home/systems/xelf/generics.fasc"))

;; ;; Swank

(format t "Preparing swank~%")
(pushnew "/data/data/org.lisp.ecl/app_resources/home/slime-2.14/"
         asdf:*central-registry*)
(asdf:oos 'asdf:load-op :swank :verbose t)
(swank-loader:init :load-contribs t :setup t :delete t :quiet t)

;; The following "patches" swank to work correctly on android/iOS
(in-package :swank/backend)
(defimplementation lisp-implementation-program ()
  "Return the argv[0] of the running Lisp process, or NIL."
  "org.lisp.ecl")

#+(or)(in-package :swank)
#+(or)(defun repl-input-stream-read (connection stdin)
  (loop
   (let* ((socket (connection.socket-io connection))
          (inputs (list socket #+(or) stdin))
          (ready (wait-for-input inputs)))
     (cond ((eq ready :interrupt)
            (check-slime-interrupts))
           ((member socket ready)
            ;; A Slime request from Emacs is pending; make sure to
            ;; redirect IO to the REPL buffer.
            (with-simple-restart (process-input "Continue reading input.")
              (let ((*sldb-quit-restart* (find-restart 'process-input)))
                (with-io-redirection (connection)
                  (handle-requests connection t)))))
           ((member stdin ready)
            ;; User typed something into the  *inferior-lisp* buffer,
            ;; so do not redirect.
            (return (read-non-blocking stdin)))
           (t (assert (null ready)))))))

(in-package :cl-user)

(defun start-swank ()
  (format t "Starting swank server~%")
  "SLIME-listener"
    (let ((swank::*loopback-interface* "0.0.0.0"))
      (swank:create-server :port 4005
			   :dont-close t
			   :style nil
			   )))

(defun stop-swank ()
  (format t "Stopping swank server~%")
  (swank:stop-server 4005)
  (format t ";; Swank off-line~%"))

(require '#:ecl-quicklisp)
(require '#:deflate)
(require '#:ql-minitar)

(defun load-sdl ()
  (ql:register-local-projects)
  (ql:quickload '(:alexandria :babel :trivial-garbage))
  (asdf:load-system :cffi)
  (asdf:load-system :cl-opengl)
  (ql:quickload :cl-fad)
  (ql:quickload :lispbuilder-sdl)
  (ql:quickload :lispbuilder-sdl-image)
  (ql:quickload :lispbuilder-sdl-mixer)
  (ql:quickload :lispbuilder-sdl-gfx)
  (ql:quickload :lispbuilder-sdl-ttf))

(defun load-benchmark ()
  (load "/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/trivial-benchmark/package.fas")
  (load "/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/trivial-benchmark/toolkit.fas")
  (load "/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/trivial-benchmark/timer.fas")
  (load "/data/data/org.lisp.ecl/app_resources/home/quicklisp/local-projects/trivial-benchmark/samples.fas"))

(defun load-base-fascs ()
  (handler-case
      (handler-bind ((warning #'muffle-warning))
	(load-fascs *fascs*))
    (condition (c)
      (invoke-debugger c))))

(defun load-inlined-generic-function ()
  (asdf:load-system :inlined-generic-function))

(defun load-base-systems ()
  (handler-case
      (handler-bind ((warning #'muffle-warning))
	(progn
	  (push :cl-opengl-no-check-error *features*)
	  (load-sdl)
	  (push :inline-generic-function *features*)
	  (load-inlined-generic-function)
	  ;;(load-benchmark)
	  ;; load precompiled native xelf core 
	  (load "/data/data/org.lisp.ecl/app_resources/home/systems/quadrille/package.lisp" :verbose t)
	  (load "/data/data/org.lisp.ecl/app_resources/home/systems/quadrille/quadrille.fas" :verbose t)
	  (asdf:load-system :xelf)))
    (error (c)
      (invoke-debugger c))))

(defun load-gl (&optional fallback)
  (push :cl-opengl-no-check-error *features*)
  (if fallback
      (load-fascs *gl-fascs*)
      (load "/data/data/org.lisp.ecl/app_resources/home/systems/cl-opengl.fasb" :verbose t)))

(defun load-bundles (&optional fallback)
  (setf *features* (delete :long-long *features*))
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/alexandria.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/babel.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/trivial-garbage.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/trivial-features.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/bordeaux-threads.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/cl-fad.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/cffi.fasb" :verbose t)
  (load-gl fallback)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/quadrille.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-cffi.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-base.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-assets.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-binaries.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-image-cffi.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-image.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-image-binaries.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-gfx-cffi.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-gfx.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-mixer-cffi.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-mixer-binaries.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-mixer.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-ttf-binaries.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-ttf-cffi.fasb" :verbose t)
  (load "/data/data/org.lisp.ecl/app_resources/home/systems/lispbuilder-sdl-ttf.fasb" :verbose t))

(defun load-base-systems-quickly (&optional fallback)
  (handler-case
      (handler-bind ((warning #'muffle-warning))
	(progn (load-bundles fallback)
	       (load-fascs *fascs*)
	       (load-fascs *xelf-fascs*)))
    (error (c)
      (invoke-debugger c))))

(defun reload-xelf ()
  (handler-case
      (handler-bind ((warning #'muffle-warning))
	(dolist (lisp *xelf-lisps*)
	  (load (compile-file lisp :verbose t) :verbose t)))
    (error (c)
      (invoke-debugger c))))
  
(defun load-3x0ng ()
  (handler-case (handler-bind ((warning #'muffle-warning))
		  (progn 
		    (load "/data/data/org.lisp.ecl/app_resources/home/systems/3x0ng/core.fas")
		    (load "/data/data/org.lisp.ecl/app_resources/home/systems/3x0ng/package.lisp")
		    (load "/data/data/org.lisp.ecl/app_resources/home/systems/3x0ng/3x0ng.lisp")))
    (condition (c) (invoke-debugger c))))

(defun load-all (&optional fallback)
  (load-base-systems-quickly fallback)
  (load "/data/data/org.lisp.ecl/app_resources/etc/foreign.lisp")
  (load-3x0ng))

(start-swank)


